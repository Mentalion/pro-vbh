Studentnummer: 331899
Naam: Robin Dekker

Hoe open je de Terminal in Visual Studio Code?
	# ctrl + shift + `/ boven in via het tabje "Terminal".

Met welk commando kan je checken of welke bestanden zijn toegevoegd aan de commit en welke niet?
	# git status

Wat is het commando van een multiline commit message?
	# voor elke line die je wilt begin je dan weer met -m

Hoeveel commando's heb je in opdracht 4a uitgevoerd?
	# 3

Zoek het volgende commando op:
 - 1 commit teruggaan in de commit history. (reset)
	# git reset
